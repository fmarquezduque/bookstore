

const Add = ( Itemlibro, libro ) => {
    const existe = Itemlibro.find((x) => x.id === libro.id);
    if (existe) {
      return  Itemlibro.map((x) => x.id === libro.id ? { ...existe, cantidad: existe.cantidad + 1 } : x )
    } else {
      return [...Itemlibro, { ...libro, cantidad: 1 }];
    }
};


const Remove = (Itemlibro, libro) => {
    const existe = Itemlibro.find((x) => x.id === libro.id);
    if (existe.cantidad === 1) {
      return Itemlibro.filter((x) => x.id !== libro.id)
    } else {
        return Itemlibro.map((x) => x.id === libro.id ? { ...existe, cantidad: existe.cantidad - 1 } : x)
    }
};

const reducer = ( state, action ) => {

      switch (action.type) {
        case 'ADD':
          return {
            ...state,
            cantidad: state.cantidad + 1,
            cart: Add(state.cart, action.payload),
            precio_total: state.precio_total + action.payload.precio
          }
          break;
        case 'REMOVE':
          return {
            ...state,
            cantidad: state.cantidad - 1,
            cart: Remove(state.cart, action.payload),
            precio_total: state.precio_total - action.payload.precio

          }
          break;
        default:
         return state;

      }

}

export default reducer;
