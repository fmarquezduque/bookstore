
import { useReducer } from 'react';
import {
  BrowserRouter as Router,
  Switch,
  Route
} from "react-router-dom";

import Books from './components/Books';
import Nav from './components/Nav';
import Cart from './components/Cart';
import datos from './books';
import reducer from './reducer';
import initialState from './initialState';
import './App.css';

function App() {

  const { libros } = datos;

  const [state, dispatch] = useReducer(reducer, initialState);

  const remove = ( libro ) => {
    dispatch({ type: "REMOVE", payload: libro });
  };
  const add = (libro) => {
    dispatch({ type: "ADD", payload: libro });
  };


  return (
    <Router>
      <Nav cantidad={state.cantidad}/>
      <Switch>
        <Route exact path="/">
          <Books libros = { libros } add = { add } />
        </Route>
        <Route exact path="/factura">
          <Cart state = { state } add = { add } remove = { remove } />
        </Route>
      </Switch>
    </Router>
  );
}

export default App;
