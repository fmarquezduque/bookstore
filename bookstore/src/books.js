const datos = {

    libros: [
      {
        'id':1,
        'titulo':'Programación Web',
        'precio': 2200
      },
      {
        'id':2,
        'titulo':'Spring en vacaciones',
        'precio': 1100
      },
      {
        'id':3,
        'titulo':'React de cero a experto',
        'precio': 5000
      },
      {
        'id':4,
        'titulo':'JS de cero a experto',
        'precio': 600
      },
      {
        'id':5,
        'titulo':'API',
        'precio': 1100
      },
      {
        'id':6,
        'titulo':'HTML and CSS',
        'precio': 100
      },
    ],
};

export default datos;
