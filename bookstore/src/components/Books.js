import React from 'react';
import Book from './Book'

function Books( props ) {

    const { libros, add } = props;

    return (

      <div className="container pt-5">
        <h2>Libros</h2>
        <div className="row">
          {
            libros.map((libro) => (
              <Book key = {libro.id} libro = {libro} add = {add} />
            ))
          }
        </div>
      </div>

    )

}

export default Books;
