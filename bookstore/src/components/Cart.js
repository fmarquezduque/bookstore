import React from 'react';


function Cart(props) {

  const { state , add, remove  } = props;
  console.log(state);
  return (

      <div className = "container">
        <div className="card" style={{ width: '58rem' }}>
          <div className="card-header">
            <h5>Detalle del carrito</h5>
          </div>
          <div className = "row">

            <div className = "col">
              <ul className = "list-group" >
                {
                  state.cart.map((libro) => (
                    <li className = "list-group-item" key = { libro.id }>
                      { libro.titulo }
                    </li>
                  ))
                }
              </ul>
            </div>
            <div className = "col">
              <ul className = "list-group" >
                {
                  state.cart.map((libro) => (
                    <li className = "list-group-item"  key = { libro.id }>
                      { state.precio_total >= 0 && <button onClick={(e) => remove(libro)}>-</button>}
                      <button onClick={(e) => add(libro)}>+</button>
                    </li>
                  ))
                }
              </ul>
            </div>
            <div className = "col">
              <ul className = "list-group" >
                {
                  state.cart.map((libro) => (
                    <li className = "list-group-item"  key = { libro.id }>
                      {libro.cantidad} x { libro.precio }
                    </li>
                  ))
                }
              </ul>
            </div>
            <p>Precio total : { state.precio_total } </p>

          </div>
        </div>
      </div>
  )
}

export default Cart;
