import React from 'react';
import { Link } from "react-router-dom";

function Nav( props) {

  return (

    <nav className="navbar navbar-expand-lg navbar-light bg-light">
      <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarTogglerDemo03" aria-controls="navbarTogglerDemo03" aria-expanded="false" aria-label="Toggle navigation">
        <span className="navbar-toggler-icon"></span>
      </button>
      <Link className="navbar-brand"  to  = "/">BookStores</Link>

      <div className="collapse navbar-collapse" id="navbarTogglerDemo03">
        <ul className="navbar-nav mr-auto mt-2 mt-lg-0">
          <li className="nav-item active">
            <Link className="nav-link" to = "/">Store</Link>
          </li>
        </ul>
        <form className="form-inline my-2 my-lg-0">
           <Link to = "/factura">

             {props.cantidad ? (
               <button className = "btn btn-warning"> <i className="bi bi-cart3"></i>{' '}{props.cantidad}</button>
             ) : (
              <button className = "btn btn-warning"> <i className="bi bi-cart3"></i></button>
             )}
           </Link>
        </form>
      </div>
    </nav>

  )
}

export default Nav;
