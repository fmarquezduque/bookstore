import React from 'react';


function Book( props ) {

    const { libro, add } = props;

    return(
      <div className="col-sm pt-4">
        <div className="card" style={{ width: '18rem' }}>
          <div className="card-header">
            <h5>{libro.titulo}</h5>
          </div>
          <div className="card-body">
            <p>${libro.precio}</p>
          </div>
          <button onClick={(e) => add(libro)} className="btn btn-primary">Agregar al carrito</button>
        </div>
      </div>
    )
}

export default Book;
